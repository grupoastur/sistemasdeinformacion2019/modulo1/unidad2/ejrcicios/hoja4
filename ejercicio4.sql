﻿USE ciclistas;

-- 1 nombre y edad de los ciclistas que han ganado etapas
     SELECT c.nombre, c.edad FROM ciclista c;   
     SELECT c1.*, e.numetapa FROM (SELECT c.nombre, c.edad, c.dorsal FROM ciclista c) c1 INNER JOIN etapa e ON c1.dorsal= e.dorsal;

-- 2 nombre y edad de los ciclistas que han ganado puertos  
     SELECT DISTINCT nombre, edad FROM ciclista c INNER JOIN puerto p ON  p.dorsal = c.dorsal;

-- 3 nombre y edad de los ciclistas que han ganado etapas y puertos
      SELECT DISTINCT nombre, edad FROM(ciclista INNER JOIN etapa e ON ciclista.dorsal = e.dorsal) INNER JOIN puerto p ON ciclista.dorsal = p.dorsal;   

-- 4 listar el director de los equpos que tengan ciclistas que hayan ganado alguna etapa
     SELECT DISTINCT e.director FROM equipo e INNER JOIN (ciclista c INNER JOIN etapa e1 ON c.dorsal = e1.dorsal) ON e.nomequipo= c.nomequipo;
  
-- 5 dorsal y nombre de los ciclistas que hayan llevado algun maillot
      SELECT DISTINCT c.dorsal, c.nombre FROM ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal; 
      SELECT DISTINCT c.dorsal, c.nombre  FROM (ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal) INNER JOIN maillot m ON l.código = m.código;
  
-- 6  dorsal y nombre de los ciclistas que hayan llevado algun maillot amarillo
      SELECT DISTINCT c.dorsal, c.nombre FROM (ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal) INNER JOIN maillot m ON l.código = m.código WHERE m.color='amarillo';
      SELECT DISTINCT c.dorsal, c.nombre FROM maillot m INNER JOIN(ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal) ON m.código= l.código WHERE m.color='amarillo';
      
-- 7 dorsal de los ciclistas que hayan llevado algun maillot y que han ganado etapas
      SELECT DISTINCT e.dorsal FROM (etapa e JOIN lleva l ON e.dorsal = l.dorsal) JOIN maillot m ON l.código = m.código; 
       -- para poder meter la tabla ciclista y asi meter el nombre, etc.
        SELECT DISTINCT c1.* FROM (SELECT DISTINCT e.dorsal FROM (etapa e JOIN lleva l ON e.dorsal = l.dorsal) JOIN maillot m ON l.código = m.código) c1 JOIN ciclista c ON c.dorsal= c1.dorsal;    

-- 8 indicar el numero de etapa de las etapas que tengan puertos
  -- haciendo join
     SELECT DISTINCT e.numetapa FROM etapa e INNER JOIN puerto p USING (numetapa) WHERE e.numetapa= p.numetapa;
   -- sin hacer join y listando las etapas que son
     SELECT DISTINCT p.numetapa FROM puerto p;
     SELECT COUNT(*)  FROM(SELECT DISTINCT p.numetapa FROM puerto p) c1;

-- 9 indicar los kilometros de las etapas que hayan ganado ciclistas del banesto y que tengan puertos
      SELECT e.kms, e.numetapa, c.nombre FROM ciclista c INNER JOIN etapa e ON c.dorsal = e.dorsal 
      INNER JOIN puerto p ON e.numetapa = p.numetapa WHERE c.nomequipo='Banesto';

-- 10 listar el numero de ciclistas que hayan ganado alguna etapa con puerto
      SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa = p.numetapa;   
      SELECT COUNT( c1.dorsal) FROM (SELECT DISTINCT e.dorsal FROM etapa e INNER JOIN puerto p ON e.numetapa = p.numetapa) c1 ;
        
-- 11 indicar el nombre de los puertos que hayan sido ganados por ciclistas del banesto
      SELECT p.nompuerto, c.nomequipo, c.nombre FROM puerto p INNER JOIN ciclista c ON p.dorsal = c.dorsal WHERE c.nomequipo='Banesto'; 
  
-- 12 listar el numero de etapas que tengan puerto, que hayan sido ganados por ciclistas del banesto con mas de 200 km
      SELECT DISTINCT p.numetapa, c.nombre, e.kms, p.nompuerto, c.dorsal FROM (puerto p INNER JOIN etapa e ON p.numetapa = e.numetapa)
      INNER JOIN ciclista c ON p.dorsal = c.dorsal WHERE c.nomequipo='Banesto' AND e.kms>100;
